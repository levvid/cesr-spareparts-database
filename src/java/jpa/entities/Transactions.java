/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Gibson Levvid
 */
@Entity
@Table(name = "Transactions")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Transactions.findAll", query = "SELECT t FROM Transactions t"),
    @NamedQuery(name = "Transactions.findByTransactionID", query = "SELECT t FROM Transactions t WHERE t.transactionID = :transactionID"),
    @NamedQuery(name = "Transactions.findByPartID", query = "SELECT t FROM Transactions t WHERE t.partID = :partID"),
    @NamedQuery(name = "Transactions.findByUserID", query = "SELECT t FROM Transactions t WHERE t.userID = :userID"),
    @NamedQuery(name = "Transactions.findByLocationID", query = "SELECT t FROM Transactions t WHERE t.locationID = :locationID"),
    @NamedQuery(name = "Transactions.findBySubLocation", query = "SELECT t FROM Transactions t WHERE t.subLocation = :subLocation"),
    @NamedQuery(name = "Transactions.findBySignOut", query = "SELECT t FROM Transactions t WHERE t.signOut = :signOut"),
    @NamedQuery(name = "Transactions.findBySignIn", query = "SELECT t FROM Transactions t WHERE t.signIn = :signIn"),
    @NamedQuery(name = "Transactions.findByDate", query = "SELECT t FROM Transactions t WHERE t.date = :date"),
    @NamedQuery(name = "Transactions.findByComment", query = "SELECT t FROM Transactions t WHERE t.comment = :comment")})
public class Transactions implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "TransactionID")
    private Integer transactionID;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PartID")
    private int partID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "UserID")
    private String userID;
    @Size(max = 25)
    @Column(name = "LocationID")
    private String locationID;
    @Size(max = 75)
    @Column(name = "SubLocation")
    private String subLocation;
    @Column(name = "SignOut")
    private Integer signOut;
    @Column(name = "SignIn")
    private Integer signIn;
    @Column(name = "Date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    @Size(max = 254)
    @Column(name = "Comment")
    private String comment;

    public Transactions() {
    }

    public Transactions(Integer transactionID) {
        this.transactionID = transactionID;
    }

    public Transactions(Integer transactionID, int partID, String userID) {
        this.transactionID = transactionID;
        this.partID = partID;
        this.userID = userID;
    }

    public Integer getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(Integer transactionID) {
        this.transactionID = transactionID;
    }

    public int getPartID() {
        return partID;
    }

    public void setPartID(int partID) {
        this.partID = partID;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getLocationID() {
        return locationID;
    }

    public void setLocationID(String locationID) {
        this.locationID = locationID;
    }

    public String getSubLocation() {
        return subLocation;
    }

    public void setSubLocation(String subLocation) {
        this.subLocation = subLocation;
    }

    public Integer getSignOut() {
        return signOut;
    }

    public void setSignOut(Integer signOut) {
        this.signOut = signOut;
    }

    public Integer getSignIn() {
        return signIn;
    }

    public void setSignIn(Integer signIn) {
        this.signIn = signIn;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (transactionID != null ? transactionID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Transactions)) {
            return false;
        }
        Transactions other = (Transactions) object;
        if ((this.transactionID == null && other.transactionID != null) || (this.transactionID != null && !this.transactionID.equals(other.transactionID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.Transactions[ transactionID=" + transactionID + " ]";
    }
    
}
