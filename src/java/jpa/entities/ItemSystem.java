/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa.entities;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Gibson Levvid
 */
@Entity
@Table(name = "ItemSystem")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ItemSystem.findAll", query = "SELECT i FROM ItemSystem i"),
    @NamedQuery(name = "ItemSystem.findByItemid", query = "SELECT i FROM ItemSystem i WHERE i.itemSystemPK.itemid = :itemid"),
    @NamedQuery(name = "ItemSystem.findBySystem", query = "SELECT i FROM ItemSystem i WHERE i.itemSystemPK.system = :system"),
    @NamedQuery(name = "ItemSystem.findBySubSystem", query = "SELECT i FROM ItemSystem i WHERE i.itemSystemPK.subSystem = :subSystem")})
public class ItemSystem implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ItemSystemPK itemSystemPK;

    public ItemSystem() {
    }

    public ItemSystem(ItemSystemPK itemSystemPK) {
        this.itemSystemPK = itemSystemPK;
    }

    public ItemSystem(int itemid, String system, String subSystem) {
        this.itemSystemPK = new ItemSystemPK(itemid, system, subSystem);
    }

    public ItemSystemPK getItemSystemPK() {
        return itemSystemPK;
    }

    public void setItemSystemPK(ItemSystemPK itemSystemPK) {
        this.itemSystemPK = itemSystemPK;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (itemSystemPK != null ? itemSystemPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ItemSystem)) {
            return false;
        }
        ItemSystem other = (ItemSystem) object;
        if ((this.itemSystemPK == null && other.itemSystemPK != null) || (this.itemSystemPK != null && !this.itemSystemPK.equals(other.itemSystemPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.ItemSystem[ itemSystemPK=" + itemSystemPK + " ]";
    }
    
}
