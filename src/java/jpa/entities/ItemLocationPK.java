/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Gibson Levvid
 */
@Embeddable
public class ItemLocationPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "ITEMID")
    private int itemid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "LOCATION")
    private String location;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 75)
    @Column(name = "SUBLOCATION")
    private String sublocation;

    public ItemLocationPK() {
    }

    public ItemLocationPK(int itemid, String location, String sublocation) {
        this.itemid = itemid;
        this.location = location;
        this.sublocation = sublocation;
    }

    public int getItemid() {
        return itemid;
    }

    public void setItemid(int itemid) {
        this.itemid = itemid;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getSublocation() {
        return sublocation;
    }

    public void setSublocation(String sublocation) {
        this.sublocation = sublocation;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) itemid;
        hash += (location != null ? location.hashCode() : 0);
        hash += (sublocation != null ? sublocation.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ItemLocationPK)) {
            return false;
        }
        ItemLocationPK other = (ItemLocationPK) object;
        if (this.itemid != other.itemid) {
            return false;
        }
        if ((this.location == null && other.location != null) || (this.location != null && !this.location.equals(other.location))) {
            return false;
        }
        if ((this.sublocation == null && other.sublocation != null) || (this.sublocation != null && !this.sublocation.equals(other.sublocation))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.ItemLocationPK[ itemid=" + itemid + ", location=" + location + ", sublocation=" + sublocation + " ]";
    }
    
}
