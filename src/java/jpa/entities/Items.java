/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Gibson Levvid
 */
@Entity
@Table(name = "Items")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Items.findAll", query = "SELECT i FROM Items i"),
    @NamedQuery(name = "Items.findByItemid", query = "SELECT i FROM Items i WHERE i.itemid = :itemid"),
    @NamedQuery(name = "Items.findByItemname", query = "SELECT i FROM Items i WHERE i.itemname = :itemname"),
    @NamedQuery(name = "Items.findByDescription", query = "SELECT i FROM Items i WHERE i.description = :description"),
    @NamedQuery(name = "Items.findByModelnrMa", query = "SELECT i FROM Items i WHERE i.modelnrMa = :modelnrMa"),
    @NamedQuery(name = "Items.findByNotes", query = "SELECT i FROM Items i WHERE i.notes = :notes"),
    @NamedQuery(name = "Items.findByStoreuntil", query = "SELECT i FROM Items i WHERE i.storeuntil = :storeuntil"),
    @NamedQuery(name = "Items.findByReevaluate", query = "SELECT i FROM Items i WHERE i.reevaluate = :reevaluate"),
    @NamedQuery(name = "Items.findByMinquantity", query = "SELECT i FROM Items i WHERE i.minquantity = :minquantity"),
    @NamedQuery(name = "Items.findByUnits", query = "SELECT i FROM Items i WHERE i.units = :units"),
    @NamedQuery(name = "Items.findByDate", query = "SELECT i FROM Items i WHERE i.date = :date"),
    @NamedQuery(name = "Items.findByPoNumber", query = "SELECT i FROM Items i WHERE i.poNumber = :poNumber"),
    @NamedQuery(name = "Items.findBySerialNum", query = "SELECT i FROM Items i WHERE i.serialNum = :serialNum"),
    @NamedQuery(name = "Items.findByOthernumber", query = "SELECT i FROM Items i WHERE i.othernumber = :othernumber")})
public class Items implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ITEMID")
    private Integer itemid;
    @Size(max = 94)
    @Column(name = "ITEMNAME")
    private String itemname;
    @Size(max = 254)
    @Column(name = "DESCRIPTION")
    private String description;
    @Size(max = 254)
    @Column(name = "MODELNR_MA")
    private String modelnrMa;
    @Size(max = 254)
    @Column(name = "NOTES")
    private String notes;
    @Column(name = "STOREUNTIL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date storeuntil;
    @Column(name = "REEVALUATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date reevaluate;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "MINQUANTITY")
    private Double minquantity;
    @Size(max = 12)
    @Column(name = "UNITS")
    private String units;
    @Column(name = "DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    @Size(max = 36)
    @Column(name = "PO_NUMBER")
    private String poNumber;
    @Size(max = 254)
    @Column(name = "SERIAL_NUM")
    private String serialNum;
    @Size(max = 254)
    @Column(name = "OTHERNUMBER")
    private String othernumber;

    public Items() {
    }

    public Items(Integer itemid) {
        this.itemid = itemid;
    }

    public Integer getItemid() {
        return itemid;
    }

    public void setItemid(Integer itemid) {
        this.itemid = itemid;
    }

    public String getItemname() {
        return itemname;
    }

    public void setItemname(String itemname) {
        this.itemname = itemname;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getModelnrMa() {
        return modelnrMa;
    }

    public void setModelnrMa(String modelnrMa) {
        this.modelnrMa = modelnrMa;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Date getStoreuntil() {
        return storeuntil;
    }

    public void setStoreuntil(Date storeuntil) {
        this.storeuntil = storeuntil;
    }

    public Date getReevaluate() {
        return reevaluate;
    }

    public void setReevaluate(Date reevaluate) {
        this.reevaluate = reevaluate;
    }

    public Double getMinquantity() {
        return minquantity;
    }

    public void setMinquantity(Double minquantity) {
        this.minquantity = minquantity;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getPoNumber() {
        return poNumber;
    }

    public void setPoNumber(String poNumber) {
        this.poNumber = poNumber;
    }

    public String getSerialNum() {
        return serialNum;
    }

    public void setSerialNum(String serialNum) {
        this.serialNum = serialNum;
    }

    public String getOthernumber() {
        return othernumber;
    }

    public void setOthernumber(String othernumber) {
        this.othernumber = othernumber;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (itemid != null ? itemid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Items)) {
            return false;
        }
        Items other = (Items) object;
        if ((this.itemid == null && other.itemid != null) || (this.itemid != null && !this.itemid.equals(other.itemid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.Items[ itemid=" + itemid + " ]";
    }
    
}
