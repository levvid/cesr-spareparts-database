/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Gibson Levvid
 */
@Embeddable
public class ItemSystemPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "ITEMID")
    private int itemid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 75)
    @Column(name = "SYSTEM")
    private String system;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 52)
    @Column(name = "SUB_SYSTEM")
    private String subSystem;

    public ItemSystemPK() {
    }

    public ItemSystemPK(int itemid, String system, String subSystem) {
        this.itemid = itemid;
        this.system = system;
        this.subSystem = subSystem;
    }

    public int getItemid() {
        return itemid;
    }

    public void setItemid(int itemid) {
        this.itemid = itemid;
    }

    public String getSystem() {
        return system;
    }

    public void setSystem(String system) {
        this.system = system;
    }

    public String getSubSystem() {
        return subSystem;
    }

    public void setSubSystem(String subSystem) {
        this.subSystem = subSystem;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) itemid;
        hash += (system != null ? system.hashCode() : 0);
        hash += (subSystem != null ? subSystem.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ItemSystemPK)) {
            return false;
        }
        ItemSystemPK other = (ItemSystemPK) object;
        if (this.itemid != other.itemid) {
            return false;
        }
        if ((this.system == null && other.system != null) || (this.system != null && !this.system.equals(other.system))) {
            return false;
        }
        if ((this.subSystem == null && other.subSystem != null) || (this.subSystem != null && !this.subSystem.equals(other.subSystem))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.ItemSystemPK[ itemid=" + itemid + ", system=" + system + ", subSystem=" + subSystem + " ]";
    }
    
}
