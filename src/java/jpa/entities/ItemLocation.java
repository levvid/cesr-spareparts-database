/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Gibson Levvid
 */
@Entity
@Table(name = "Item_Location")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ItemLocation.findAll", query = "SELECT i FROM ItemLocation i"),
    @NamedQuery(name = "ItemLocation.findByItemid", query = "SELECT i FROM ItemLocation i WHERE i.itemLocationPK.itemid = :itemid"),
    @NamedQuery(name = "ItemLocation.findByLocation", query = "SELECT i FROM ItemLocation i WHERE i.itemLocationPK.location = :location"),
    @NamedQuery(name = "ItemLocation.findBySublocation", query = "SELECT i FROM ItemLocation i WHERE i.itemLocationPK.sublocation = :sublocation"),
    @NamedQuery(name = "ItemLocation.findByContact", query = "SELECT i FROM ItemLocation i WHERE i.contact = :contact"),
    @NamedQuery(name = "ItemLocation.findByQuantity", query = "SELECT i FROM ItemLocation i WHERE i.quantity = :quantity")})
public class ItemLocation implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ItemLocationPK itemLocationPK;
    @Size(max = 50)
    @Column(name = "CONTACT")
    private String contact;
    @Column(name = "QUANTITY")
    private Integer quantity;

    public ItemLocation() {
    }

    public ItemLocation(ItemLocationPK itemLocationPK) {
        this.itemLocationPK = itemLocationPK;
    }

    public ItemLocation(int itemid, String location, String sublocation) {
        this.itemLocationPK = new ItemLocationPK(itemid, location, sublocation);
    }

    public ItemLocationPK getItemLocationPK() {
        return itemLocationPK;
    }

    public void setItemLocationPK(ItemLocationPK itemLocationPK) {
        this.itemLocationPK = itemLocationPK;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (itemLocationPK != null ? itemLocationPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ItemLocation)) {
            return false;
        }
        ItemLocation other = (ItemLocation) object;
        if ((this.itemLocationPK == null && other.itemLocationPK != null) || (this.itemLocationPK != null && !this.itemLocationPK.equals(other.itemLocationPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.ItemLocation[ itemLocationPK=" + itemLocationPK + " ]";
    }
    
}
