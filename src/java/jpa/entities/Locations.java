/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Gibson Levvid
 */
@Entity
@Table(name = "Locations")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Locations.findAll", query = "SELECT l FROM Locations l"),
    @NamedQuery(name = "Locations.findByLocationID", query = "SELECT l FROM Locations l WHERE l.locationID = :locationID"),
    @NamedQuery(name = "Locations.findByBuilding", query = "SELECT l FROM Locations l WHERE l.building = :building"),
    @NamedQuery(name = "Locations.findByLocDescription", query = "SELECT l FROM Locations l WHERE l.locDescription = :locDescription"),
    @NamedQuery(name = "Locations.findByContactGroup", query = "SELECT l FROM Locations l WHERE l.contactGroup = :contactGroup"),
    @NamedQuery(name = "Locations.findByPhoneNr", query = "SELECT l FROM Locations l WHERE l.phoneNr = :phoneNr")})
public class Locations implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "LocationID")
    private String locationID;
    @Size(max = 50)
    @Column(name = "Building")
    private String building;
    @Size(max = 50)
    @Column(name = "Loc_Description")
    private String locDescription;
    @Size(max = 20)
    @Column(name = "ContactGroup")
    private String contactGroup;
    @Size(max = 12)
    @Column(name = "PhoneNr")
    private String phoneNr;

    public Locations() {
    }

    public Locations(String locationID) {
        this.locationID = locationID;
    }

    public String getLocationID() {
        return locationID;
    }

    public void setLocationID(String locationID) {
        this.locationID = locationID;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getLocDescription() {
        return locDescription;
    }

    public void setLocDescription(String locDescription) {
        this.locDescription = locDescription;
    }

    public String getContactGroup() {
        return contactGroup;
    }

    public void setContactGroup(String contactGroup) {
        this.contactGroup = contactGroup;
    }

    public String getPhoneNr() {
        return phoneNr;
    }

    public void setPhoneNr(String phoneNr) {
        this.phoneNr = phoneNr;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (locationID != null ? locationID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Locations)) {
            return false;
        }
        Locations other = (Locations) object;
        if ((this.locationID == null && other.locationID != null) || (this.locationID != null && !this.locationID.equals(other.locationID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.Locations[ locationID=" + locationID + " ]";
    }
    
}
