package jsf;

import jpa.entities.Transactions;
import jsf.util.JsfUtil;
import jsf.util.PaginationHelper;
import jpa.session.TransactionsFacade;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import jpa.entities.ItemLocation;
import jpa.session.ItemLocationFacade;

@Named("transactionsController")
@SessionScoped
public class TransactionsController implements Serializable {

    private Transactions current;
    private DataModel items = null;
    @EJB
    private jpa.session.TransactionsFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;

    public TransactionsController() {
    }

    public Transactions getSelected() {
        if (current == null) {
            current = new Transactions();
            selectedItemIndex = -1;
        }
        return current;
    }

    private TransactionsFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(30) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (Transactions) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new Transactions();
        selectedItemIndex = -1;
        return "Create";
    }

    public String create() {
        try {
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("TransactionsCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (Transactions) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("TransactionsUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (Transactions) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("TransactionsDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public Transactions getTransactions(java.lang.Integer id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass = Transactions.class)
    public static class TransactionsControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            TransactionsController controller = (TransactionsController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "transactionsController");
            return controller.getTransactions(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Transactions) {
                Transactions o = (Transactions) object;
                return getStringKey(o.getTransactionID());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Transactions.class.getName());
            }
        }

    }
    
    
    /*********************Additional code to enable search and better page navigation*************/
    private int search = 0;
    private int first=0;
    private int last=0;
    private int listLength = 0;
    List tableDataList;
    @PersistenceContext
    private EntityManager entityManager;
    
    /********************************Pagination*********************************/
    public String backToTable(){
        recreateModel();
        getItems();
        first =0;
        search = 0;
        return "List";
    } 
    
     public String nextSearchItems(){
        first = first+30;
        
        if(last+30>listLength()){
            last = listLength();
        }
        else{
            last = last+30;
        }
        items.setWrappedData(tableDataList.subList(first, last-1));
        return "List";
    }
    
    public String previousSearchItems(){
        first = first-30;
        last = last- 30;
        items.setWrappedData(tableDataList.subList(first, last-1));
        return "List";
    }
    
    /*********************************Search************************************/
    /*Helper methods*/
    public int search(){
        return search;
    }
    
    public int first(){
        return first;
    }
     
     
    public int last(){
        return last;
    }
     
    public int listLength(){
         return listLength;
    }
    
    public boolean searchHasPrevious(){
        return first>29;
    }
    
    public boolean searchHasNext(){
        return last<listLength();
    }
    
    /*Returns true when s is a string, false when it is a number*/
     public boolean isString(String s){
         try{
            Integer.parseInt(s);
            return false;
         }
         catch(NumberFormatException e){
             return true;
         }
     }
     
     
     /**Helper methods**/
      public String analyseSearchString(String field, String input, String type){
        String search = "", parameter=" AND";
        
        String[] longerSearch = input.trim().split(" ");
        if(longerSearch.length>1&&((input.trim().indexOf("\"")!=0)||
                (input.trim().indexOf("\"")!=input.length()-1))){
            for(int i=0; i<longerSearch.length;i++){
                if(i==longerSearch.length-1){
                    search +=" i." + field+ " LIKE " + "'%" + longerSearch[i] +"%' " + type; 
                }
                else{
                    search +=" i." + field + " LIKE " + "'%" + longerSearch[i] +"%' " + parameter;
                }
            }
        }
        else{
            search += " i." + field + " LIKE " + "'%" + input +"%' " + type;
        }
       
        return search;
     }
     
     /**End of helpers**/
     /*Main search methods*/
    public String basicSearch(String transactionid, String partid, String userid,
            String locationid, String sublocation, String signout, String signin, 
            String date, String comment){
        //check whether search string is empty, refresh if empty
        String trid = transactionid.replaceAll("[\\s\\u00A0]+$", ""); 
        String pid = partid.replaceAll("[\\s\\u00A0]+$", "");
        String uid = userid.replaceAll("[\\s\\u00A0]+$", "");
        String lid = locationid.replaceAll("[\\s\\u00A0]+$", "");
        String sub = sublocation.replaceAll("[\\s\\u00A0]+$", "");
        String sout = signout.replaceAll("[\\s\\u00A0]+$", "");
        String sin = signin.replaceAll("[\\s\\u00A0]+$", "");
        String dat = date.replaceAll("[\\s\\u00A0]+$", "");
        String com = comment.replaceAll("[\\s\\u00A0]+$", "");
        
        if((trid.length()==0) && (pid.length()==0) && (uid.length()==0) 
                && (lid.length()==0)&& (sub.length()==0) && (sout.length()==0)
                && (sin.length()==0)&& (dat.length()==0)&& (com.length()==0)){
            recreateModel();
            getItems();
            first =0;
            search =0;
            return "List";
        }
        search =1;            
        normalSearch(trid, pid, uid, lid, sub, sout, sin, dat, com,  
                "AND", "AND", "AND", "AND", "AND", "AND", "AND", "AND");
             
         
        return "List";
    }
    
    public void normalSearch(String transactionid, String partid, String userid,
            String locationid, String sublocation, String signout, String signin, 
            String date, String comment,  
            String type1, String type2, String type3, String type4, String type5,
             String type6, String type7, String type8){
         String search = "SELECT i FROM Transactions i WHERE ";
       
         if(transactionid.trim().length()>0){
             search += analyseSearchString("transactionID", transactionid, type1);
         }
         
         if(partid.trim().length()>0){
             search += analyseSearchString("partID", partid, type2);
         }
         
         if(userid.trim().length()>0){
             search += analyseSearchString("userID", userid, type3);
         }
         
         if(locationid.trim().length()>0){
             search += analyseSearchString("locationID", locationid, type4);
         }
         
         if(sublocation.trim().length()>0){
             search += analyseSearchString("subLocation", sublocation, type5);
         }
         
         if(signout.trim().length()>0){
             search += analyseSearchString("signOut", signout, type6);
         }
         
         if(signin.trim().length()>0){
             search += analyseSearchString("signIn", signin, type7);
         }
         
         if(date.trim().length()>0){
             search += analyseSearchString("date", date, type8);
         }
         
         if(comment.trim().length()>0){
             search += analyseSearchString("comment", comment, "");
         }
         
         
         
         
        if(search.trim().endsWith("OR")){
            search = search.trim().substring(0,search.length()-3);
        }
        if(search.trim().endsWith("AND")){
            search = search.trim().substring(0,search.length()-4);
        }
        
        
        tableDataList = entityManager.createQuery(search).getResultList();//set the page data model
        listLength = tableDataList.size();
        if(listLength>30){
            last=30;
        }
        else{
            last = listLength();
        }
        int tmpLast=0;
        if(listLength()>30){
            tmpLast=29;
        }
        else{
            tmpLast = listLength();
        }
        items.setWrappedData(tableDataList.subList(0, tmpLast));
        recreatePagination();
     }
   
    
    
    
    /**Administrator privileges methods**/
    public boolean isAdmin(){
        
        return true;
    }
    /*End of admin privileges methods*/
    
    
    
    /***************Sign in/out*******************/
    public String saveSignInOut(String partid, String userid,
            String locationid, String sublocation, String signout, String signin, 
            String date, String comment, String quantity, 
            ItemLocation itm, ItemLocationFacade ilFacade){
        Transactions signInOut = new Transactions();
        selectedItemIndex = -1;
        int qty = getInteger(quantity), signOut = getInteger(signout),
                signIn = getInteger(signin);
        signInOut.setPartID(getInteger(partid));
        signInOut.setUserID(userid);
        signInOut.setLocationID(locationid);
        signInOut.setSubLocation(sublocation);
        signInOut.setSignOut(getInteger(signout));
        signInOut.setSignIn(getInteger(signin));
        signInOut.setDate(getDate(date));
        signInOut.setComment(comment);
        itm.setQuantity(qty+signIn-signOut);
        try {
            getFacade().create(signInOut);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("TransactionsCreated"));
            current = new Transactions();
            selectedItemIndex = -1;
            try{
               ilFacade.edit(itm);
            }
            catch(Exception e){
                JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
            }
            
            
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
            
            return null;
        }
        
        
    }
    
    
    
    public int getInteger(String input){
        try{
            int converted = Integer.parseInt(input);
            return converted;
        }catch(Exception e){
            if(input.trim().length()==0){
                return 0;
            }else{
                JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("Please enter a number"));
                return -1;
            }
            
        }
    }
    
    public Date getDate(String userInput){
        String expectedPattern = "MM/dd/yyyy";
        SimpleDateFormat formatter = new SimpleDateFormat(expectedPattern);
        try
        {
          Date date = formatter.parse(userInput);
          return date;
        }
        catch (ParseException e)
        {
          e.printStackTrace();
          return null;
        }
        
    }
    
}
