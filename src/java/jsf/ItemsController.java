package jsf;

import jpa.entities.Items;
import jsf.util.JsfUtil;
import jsf.util.PaginationHelper;
import jpa.session.ItemsFacade;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Named("itemsController")
@SessionScoped
public class ItemsController implements Serializable {

    private Items current;
    private DataModel items = null;
    @EJB
    private jpa.session.ItemsFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;

    public ItemsController() {
    }

    public Items getSelected() {
        if (current == null) {
            current = new Items();
            selectedItemIndex = -1;
        }
        return current;
    }

    private ItemsFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(30) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (Items) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        resetSignInOut();
        return "View";
    }

    public String prepareCreate() {
        current = new Items();
        selectedItemIndex = -1;
        resetSignInOut();
        return "Create";
    }

    public String create() {
        try {
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("ItemsCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (Items) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        resetSignInOut();
        return "Edit";
    }

    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("ItemsUpdated"));
            resetSignInOut();
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (Items) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("ItemsDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public Items getItems(java.lang.Integer id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass = Items.class)
    public static class ItemsControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            ItemsController controller = (ItemsController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "itemsController");
            return controller.getItems(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Items) {
                Items o = (Items) object;
                return getStringKey(o.getItemid());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Items.class.getName());
            }
        }

    }
    
    
    
    /*********************Additional code to enable search and better page navigation*************/
    private int search = 0;
    private int first=0;
    private int last=0;
    private int listLength = 0;
    private boolean sign = false;
    List tableDataList;
    @PersistenceContext
    private EntityManager entityManager;
    
    /********************************Pagination*********************************/
    public String backToTable(){
        recreateModel();
        getItems();
        first =0;
        search = 0;
        return "List";
    } 
    
     public String nextSearchItems(){
        first = first+30;
        
        if(last+30>listLength()){
            last = listLength();
        }
        else{
            last = last+30;
        }
        items.setWrappedData(tableDataList.subList(first, last-1));
        return "List";
    }
    
    public String previousSearchItems(){
        first = first-30;
        last = last- 30;
        items.setWrappedData(tableDataList.subList(first, last-1));
        return "List";
    }
    
    /*********************************Search************************************/
    /*Helper methods*/
    public int search(){
        return search;
    }
    
    public int first(){
        return first;
    }
     
     
    public int last(){
        return last;
    }
     
    public int listLength(){
         return listLength;
    }
    
    public boolean searchHasPrevious(){
        return first>29;
    }
    
    public boolean searchHasNext(){
        return last<listLength();
    }
    
    /*Returns true when s is a string, false when it is a number*/
     public boolean isString(String s){
         try{
            Integer.parseInt(s);
            return false;
         }
         catch(NumberFormatException e){
             return true;
         }
     }
     
     
     /**Helper methods**/
      public String analyseSearchString(String field, String input, String type){
        String search = "", parameter=" AND";
        
        String[] longerSearch = input.trim().split(" ");
        if(longerSearch.length>1&&((input.trim().indexOf("\"")!=0)||
                (input.trim().indexOf("\"")!=input.length()-1))){
            for(int i=0; i<longerSearch.length;i++){
                if(i==longerSearch.length-1){
                    search +=" i." + field+ " LIKE " + "'%" + longerSearch[i] +"%' " + type; 
                }
                else{
                    search +=" i." + field + " LIKE " + "'%" + longerSearch[i] +"%' " + parameter;
                }
            }
        }
        else{
            search += " i." + field + " LIKE " + "'%" + input +"%' " + type;
        }
       
        return search;
     }
     
     /**End of helpers**/
     /*Main search methods*/
    public String basicSearch(String itemid, String itemname, String description,
            String modelnrma, String notes, String storeuntil, String reevaluate,
            String minquantity, String units, String date, String ponumber, 
            String serialnum, String othernumber ){
        //check whether search string is empty, refresh if empty
        String itid = itemid.replaceAll("[\\s\\u00A0]+$", ""); 
        String itnm = itemname.replaceAll("[\\s\\u00A0]+$", "");
        String des = description.replaceAll("[\\s\\u00A0]+$", "");
        String mod = modelnrma.replaceAll("[\\s\\u00A0]+$", "");
        String nots = notes.replaceAll("[\\s\\u00A0]+$", "");
        String stl = storeuntil.replaceAll("[\\s\\u00A0]+$", "");
        String rev = reevaluate.replaceAll("[\\s\\u00A0]+$", "");
        String minq = minquantity.replaceAll("[\\s\\u00A0]+$", "");
        String unt = units.replaceAll("[\\s\\u00A0]+$", "");
        String dt = date.replaceAll("[\\s\\u00A0]+$", "");
        String pnm = ponumber.replaceAll("[\\s\\u00A0]+$", "");
        String snm = serialnum.replaceAll("[\\s\\u00A0]+$", "");
        String onm = othernumber.replaceAll("[\\s\\u00A0]+$", "");
        
        
        if((itid.length()==0) && (itnm.length()==0) && (des.length()==0) 
                && (mod.length()==0)&& (nots.length()==0) && (stl.length()==0)
                && (rev.length()==0) && (minq.length()==0)&& (unt.length()==0)
                && (dt.length()==0) && (pnm.length()==0)&& (snm.length()==0)
                && (onm.length()==0)){
            recreateModel();
            getItems();
            first =0;
            search =0;
            return "List";
        }
        search =1;            
        normalSearch(itid, itnm, des, mod, nots, stl, rev, minq, unt, dt, pnm, snm, onm,
                "AND", "AND", "AND", "AND", "AND", "AND", "AND", "AND", 
                "AND", "AND", "AND", "AND");
             
         
        return "List";
    }
    
    public void normalSearch(String itemid, String itemname, String description,
            String modelnrma, String notes, String storeuntil, String reevaluate,
            String minquantity, String units, String date, String ponumber, 
            String serialnum, String othernumber, 
            String type1, String type2, String type3, String type4, String type5, 
            String type6, String type7, String type8, String type9, String type10, 
            String type11, String type12){
         String search = "SELECT i FROM Items i WHERE ";
       
         if(itemid.trim().length()>0){
             search += analyseSearchString("itemid", itemid, type1);
         }
         
         if(itemname.trim().length()>0){
             search += analyseSearchString("itemname", itemname, type2);
         }
         
         if(description.trim().length()>0){
             search += analyseSearchString("description", description, type3);
         }
         
         if(modelnrma.trim().length()>0){
             search += analyseSearchString("modelnrMa", modelnrma, type4);
         }
         
         if(notes.trim().length()>0){
             search += analyseSearchString("notes", notes, type5);
         }
         
         if(storeuntil.trim().length()>0){
             search += analyseSearchString("storeuntil", storeuntil, type6);
         }
         
         if(reevaluate.trim().length()>0){
             search += analyseSearchString("reevaluate", reevaluate, type7);
         }
         
         if(minquantity.trim().length()>0){
             search += analyseSearchString("minquantity", minquantity, type8);
         }
         
         if(units.trim().length()>0){
             search += analyseSearchString("units", units, type9);
         }
         
         if(date.trim().length()>0){
             search += analyseSearchString("date", date, type10);
         }
         
         if(ponumber.trim().length()>0){
             search += analyseSearchString("poNumber", ponumber, type11);
         }
         
         if(serialnum.trim().length()>0){
             search += analyseSearchString("serialNum", serialnum, type12);
         }
         
         if(othernumber.trim().length()>0){
             search += analyseSearchString("othernumber", othernumber, "");
         }
         
         
         
         
        if(search.trim().endsWith("OR")){
            search = search.trim().substring(0,search.length()-3);
        }
        if(search.trim().endsWith("AND")){
            search = search.trim().substring(0,search.length()-4);
        }
        
        
        tableDataList = entityManager.createQuery(search).getResultList();//set the page data model
        listLength = tableDataList.size();
        if(listLength>30){
            last=30;
        }
        else{
            last = listLength();
        }
        int tmpLast=0;
        if(listLength()>30){
            tmpLast=29;
        }
        else{
            tmpLast = listLength();
        }
        items.setWrappedData(tableDataList.subList(0, tmpLast));
        recreatePagination();
     }
   
    
    
    
    /**Administrator privileges methods**/
    public boolean isAdmin(){
        
        return true;
    }
    /*End of admin privileges methods*/
    
    
    /*******************Sign in and/or out methods***********************************/
    public boolean signInOut(){
        return sign;
    }
    
    public void setSignInOut(){
        sign = true;
    }
    
    public void resetSignInOut(){
        sign = false;
    }
    

}
