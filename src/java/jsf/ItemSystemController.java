package jsf;

import jpa.entities.ItemSystem;
import jsf.util.JsfUtil;
import jsf.util.PaginationHelper;
import jpa.session.ItemSystemFacade;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Named("itemSystemController")
@SessionScoped
public class ItemSystemController implements Serializable {

    private ItemSystem current;
    private DataModel items = null;
    @EJB
    private jpa.session.ItemSystemFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;

    
    @PersistenceContext
    private EntityManager entityManager;
    
    
    public ItemSystemController() {
    }

    public ItemSystem getSelected() {
        if (current == null) {
            current = new ItemSystem();
            current.setItemSystemPK(new jpa.entities.ItemSystemPK());
            selectedItemIndex = -1;
        }
        return current;
    }

    private ItemSystemFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(30) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (ItemSystem) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new ItemSystem();
        current.setItemSystemPK(new jpa.entities.ItemSystemPK());
        selectedItemIndex = -1;
        return "Create";
    }

    public String create() {
        try {
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("ItemSystemCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (ItemSystem) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("ItemSystemUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (ItemSystem) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("ItemSystemDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public ItemSystem getItemSystem(jpa.entities.ItemSystemPK id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass = ItemSystem.class)
    public static class ItemSystemControllerConverter implements Converter {

        private static final String SEPARATOR = "#";
        private static final String SEPARATOR_ESCAPED = "\\#";

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            ItemSystemController controller = (ItemSystemController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "itemSystemController");
            return controller.getItemSystem(getKey(value));
        }

        jpa.entities.ItemSystemPK getKey(String value) {
            jpa.entities.ItemSystemPK key;
            String values[] = value.split(SEPARATOR_ESCAPED);
            key = new jpa.entities.ItemSystemPK();
            key.setItemid(Integer.parseInt(values[0]));
            key.setSystem(values[1]);
            key.setSubSystem(values[2]);
            return key;
        }

        String getStringKey(jpa.entities.ItemSystemPK value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value.getItemid());
            sb.append(SEPARATOR);
            sb.append(value.getSystem());
            sb.append(SEPARATOR);
            sb.append(value.getSubSystem());
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof ItemSystem) {
                ItemSystem o = (ItemSystem) object;
                return getStringKey(o.getItemSystemPK());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + ItemSystem.class.getName());
            }
        }

    }
    
    
    /*********************Additional code to enable search and better page navigation*************/
    private int search = 0;
    private int first=0;
    private int last=0;
    private int listLength = 0;
    List tableDataList;
    
    /********************************Pagination*********************************/
    public String backToTable(){
        recreateModel();
        getItems();
        first =0;
        search = 0;
        return "List";
    } 
    
     public String nextSearchItems(){
        first = first+30;
        
        if(last+30>listLength()){
            last = listLength();
        }
        else{
            last = last+30;
        }
        items.setWrappedData(tableDataList.subList(first, last-1));
        return "List";
    }
    
    public String previousSearchItems(){
        first = first-30;
        last = last- 30;
        items.setWrappedData(tableDataList.subList(first, last-1));
        return "List";
    }
    
    /*********************************Search************************************/
    /*Helper methods*/
    public int search(){
        return search;
    }
    
    public int first(){
        return first;
    }
     
     
    public int last(){
        return last;
    }
     
    public int listLength(){
         return listLength;
    }
    
    public boolean searchHasPrevious(){
        return first>29;
    }
    
    public boolean searchHasNext(){
        return last<listLength();
    }
    
    /*Returns true when s is a string, false when it is a number*/
     public boolean isString(String s){
         try{
            Integer.parseInt(s);
            return false;
         }
         catch(NumberFormatException e){
             return true;
         }
     }
     
     
     /**Helper methods**/
      public String analyseSearchString(String field, String input, String type){
        String search = "", parameter=" AND";
        
        String[] longerSearch = input.trim().split(" ");
        if(longerSearch.length>1&&((input.trim().indexOf("\"")!=0)||
                (input.trim().indexOf("\"")!=input.length()-1))){
            for(int i=0; i<longerSearch.length;i++){
                if(i==longerSearch.length-1){
                    search +=" i." + field+ " LIKE " + "'%" + longerSearch[i] +"%' " + type; 
                }
                else{
                    search +=" i." + field + " LIKE " + "'%" + longerSearch[i] +"%' " + parameter;
                }
            }
        }
        else{
            search += " i." + field + " LIKE " + "'%" + input +"%' " + type;
        }
       
        return search;
     }
     
     /**End of helpers**/
     /*Main search methods*/
    public String basicSearch(String itemid, String subsystem, String system){
        //check whether search string is empty, refresh if empty
        String itm = itemid.replaceAll("[\\s\\u00A0]+$", ""); 
        String sub = subsystem.replaceAll("[\\s\\u00A0]+$", "");
        String sys = system.replaceAll("[\\s\\u00A0]+$", "");
        if((itm.length()==0) && (sub.length()==0) && (sys.length()==0)){
            recreateModel();
            getItems();
            first =0;
            search =0;
            return "List";
        }
        search =1;            
        normalSearch(itm, sub, sys, "AND", "AND");
             
         
        return "List";
    }
    
    public void normalSearch(String itemid, String subsystem, String system,
            String type1, String type2){
         String search = "SELECT i FROM ItemSystem i WHERE ";
       
         if(itemid.trim().length()>0){
             search += analyseSearchString("itemSystemPK.itemid", itemid, type1);
         }
         
         if(subsystem.trim().length()>0){
             search += analyseSearchString("itemSystemPK.subSystem", subsystem, type2);
         }
         
         if(system.trim().length()>0){
             search += analyseSearchString("itemSystemPK.system", system, "");
         }
         
         
         
         
        if(search.trim().endsWith("OR")){
            search = search.trim().substring(0,search.length()-3);
        }
        if(search.trim().endsWith("AND")){
            search = search.trim().substring(0,search.length()-4);
        }
        
        
        tableDataList = entityManager.createQuery(search).getResultList();//set the page data model
        listLength = tableDataList.size();
        if(listLength>30){
            last=30;
        }
        else{
            last = listLength();
        }
        int tmpLast=0;
        if(listLength()>30){
            tmpLast=29;
        }
        else{
            tmpLast = listLength();
        }
        items.setWrappedData(tableDataList.subList(0, tmpLast));
        recreatePagination();
     }
   
    
    
    
    /**Administrator privileges methods**/
    public boolean isAdmin(){
        
        return true;
    }
    /*End of admin privileges methods*/
    
    /**********************Sign in/Ou
     * @return t***************************************/
    public ItemSystem getSelecteditem(String itemId) {
        String search = "SELECT i FROM ItemSystem i WHERE i.itemSystemPK.itemid LIKE " 
                + "'%" + itemId +"%'"; 
        ItemSystem queryObject = (ItemSystem) entityManager.createQuery(search).getSingleResult();  
        return queryObject;
    }
}
