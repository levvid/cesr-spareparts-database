package jsf;

import jpa.entities.System;
import jsf.util.JsfUtil;
import jsf.util.PaginationHelper;
import jpa.session.SystemFacade;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Named("systemController")
@SessionScoped
public class SystemController implements Serializable {

    private System current;
    private DataModel items = null;
    @EJB
    private jpa.session.SystemFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;

    public SystemController() {
    }

    public System getSelected() {
        if (current == null) {
            current = new System();
            selectedItemIndex = -1;
        }
        return current;
    }

    private SystemFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(30) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (System) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new System();
        selectedItemIndex = -1;
        return "Create";
    }

    public String create() {
        try {
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("SystemCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (System) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("SystemUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (System) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("SystemDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public System getSystem(java.lang.String id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass = System.class)
    public static class SystemControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            SystemController controller = (SystemController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "systemController");
            return controller.getSystem(getKey(value));
        }

        java.lang.String getKey(String value) {
            java.lang.String key;
            key = value;
            return key;
        }

        String getStringKey(java.lang.String value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof System) {
                System o = (System) object;
                return getStringKey(o.getSystem());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + System.class.getName());
            }
        }

    }
    
    
    
    /*********************Additional code to enable search and better page navigation*************/
    private int search = 0;
    private int first=0;
    private int last=0;
    private int listLength = 0;
    List tableDataList;
    @PersistenceContext
    private EntityManager entityManager;
    
    /********************************Pagination*********************************/
    public String backToTable(){
        recreateModel();
        getItems();
        first =0;
        search = 0;
        return "List";
    } 
    
     public String nextSearchItems(){
        first = first+30;
        
        if(last+30>listLength()){
            last = listLength();
        }
        else{
            last = last+30;
        }
        items.setWrappedData(tableDataList.subList(first, last-1));
        return "List";
    }
    
    public String previousSearchItems(){
        first = first-30;
        last = last- 30;
        items.setWrappedData(tableDataList.subList(first, last-1));
        return "List";
    }
    
    /*********************************Search************************************/
    /*Helper methods*/
    public int search(){
        return search;
    }
    
    public int first(){
        return first;
    }
     
     
    public int last(){
        return last;
    }
     
    public int listLength(){
         return listLength;
    }
    
    public boolean searchHasPrevious(){
        return first>29;
    }
    
    public boolean searchHasNext(){
        return last<listLength();
    }
    
    /*Returns true when s is a string, false when it is a number*/
     public boolean isString(String s){
         try{
            Integer.parseInt(s);
            return false;
         }
         catch(NumberFormatException e){
             return true;
         }
     }
     
     
     /**Helper methods**/
      public String analyseSearchString(String field, String input, String type){
        String search = "", parameter=" AND";
        
        String[] longerSearch = input.trim().split(" ");
        if(longerSearch.length>1&&((input.trim().indexOf("\"")!=0)||
                (input.trim().indexOf("\"")!=input.length()-1))){
            for(int i=0; i<longerSearch.length;i++){
                if(i==longerSearch.length-1){
                    search +=" i." + field+ " LIKE " + "'%" + longerSearch[i] +"%' " + type; 
                }
                else{
                    search +=" i." + field + " LIKE " + "'%" + longerSearch[i] +"%' " + parameter;
                }
            }
        }
        else{
            search += " i." + field + " LIKE " + "'%" + input +"%' " + type;
        }
       
        return search;
     }
     
     /**End of helpers**/
     /*Main search methods*/
    public String basicSearch(String system){
        //check whether search string is empty, refresh if empty
        String sys = system.replaceAll("[\\s\\u00A0]+$", ""); 
        
        
        if((sys.length()==0)){
            recreateModel();
            getItems();
            first =0;
            search =0;
            return "List";
        }
        search =1;            
        normalSearch(sys);
             
         
        return "List";
    }
    
    public void normalSearch(String system){
         String search = "SELECT i FROM System i WHERE ";
       
         if(system.trim().length()>0){
             search += analyseSearchString("system", system, "");
         }
         
         
         
         
         
         
        if(search.trim().endsWith("OR")){
            search = search.trim().substring(0,search.length()-3);
        }
        if(search.trim().endsWith("AND")){
            search = search.trim().substring(0,search.length()-4);
        }
        
        
        tableDataList = entityManager.createQuery(search).getResultList();//set the page data model
        listLength = tableDataList.size();
        if(listLength>30){
            last=30;
        }
        else{
            last = listLength();
        }
        int tmpLast=0;
        if(listLength()>30){
            tmpLast=29;
        }
        else{
            tmpLast = listLength();
        }
        items.setWrappedData(tableDataList.subList(0, tmpLast));
        recreatePagination();
     }
   
    
    
    
    /**Administrator privileges methods**/
    public boolean isAdmin(){
        
        return true;
    }
    /*End of admin privileges methods*/
    


}
