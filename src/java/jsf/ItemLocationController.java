package jsf;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import jpa.entities.ItemLocation;
import jpa.session.ItemLocationFacade;
import jsf.util.JsfUtil;
import jsf.util.PaginationHelper;

@Named("itemLocationController")
@SessionScoped
public class ItemLocationController implements Serializable {

    private ItemLocation current;
    private DataModel items = null;
    @EJB
    private jpa.session.ItemLocationFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;
    
    @PersistenceContext
    private EntityManager entityManager;
    public ItemLocationController() {
    }

    public ItemLocation getSelected() {
        if (current == null) {
            current = new ItemLocation();
            current.setItemLocationPK(new jpa.entities.ItemLocationPK());
            selectedItemIndex = -1;
        }
        return current;
    }

    private ItemLocationFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(30) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (ItemLocation) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new ItemLocation();
        current.setItemLocationPK(new jpa.entities.ItemLocationPK());
        selectedItemIndex = -1;
        return "Create";
    }

    public String create() {
        try {
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("ItemLocationCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (ItemLocation) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("ItemLocationUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (ItemLocation) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("ItemLocationDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public ItemLocation getItemLocation(jpa.entities.ItemLocationPK id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass = ItemLocation.class)
    public static class ItemLocationControllerConverter implements Converter {

        private static final String SEPARATOR = "#";
        private static final String SEPARATOR_ESCAPED = "\\#";

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            ItemLocationController controller = (ItemLocationController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "itemLocationController");
            return controller.getItemLocation(getKey(value));
        }

        jpa.entities.ItemLocationPK getKey(String value) {
            jpa.entities.ItemLocationPK key;
            String values[] = value.split(SEPARATOR_ESCAPED);
            key = new jpa.entities.ItemLocationPK();
            key.setItemid(Integer.parseInt(values[0]));
            key.setLocation(values[1]);
            key.setSublocation(values[2]);
            return key;
        }

        String getStringKey(jpa.entities.ItemLocationPK value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value.getItemid());
            sb.append(SEPARATOR);
            sb.append(value.getLocation());
            sb.append(SEPARATOR);
            sb.append(value.getSublocation());
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof ItemLocation) {
                ItemLocation o = (ItemLocation) object;
                return getStringKey(o.getItemLocationPK());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + ItemLocation.class.getName());
            }
        }

    }
    
    /*********************Additional code to enable search and better page navigation*************/
    private int search = 0;
    private int first=0;
    private int last=0;
    private int listLength = 0;
    List tableDataList;
    
    /********************************Pagination*********************************/
    public String backToItemLocation(){
        recreateModel();
        getItems();
        first =0;
        search = 0;
        return "List";
    } 
    
     public String nextSearchItems(){
        first = first+30;
        
        if(last+30>listLength()){
            last = listLength();
        }
        else{
            last = last+30;
        }
        items.setWrappedData(tableDataList.subList(first, last-1));
        return "List";
    }
    
    public String previousSearchItems(){
        first = first-30;
        last = last- 30;
        items.setWrappedData(tableDataList.subList(first, last-1));
        return "List";
    }
    
    /*********************************Search************************************/
    /*Helper methods*/
    public int search(){
        return search;
    }
    
    public int first(){
        return first;
    }
     
     
    public int last(){
        return last;
    }
     
    public int listLength(){
         return listLength;
    }
    
    public boolean searchHasPrevious(){
        return first>29;
    }
    
    public boolean searchHasNext(){
        return last<listLength();
    }
    
    /*Returns true when s is a string, false when it is a number*/
     public boolean isString(String s){
         try{
            Integer.parseInt(s);
            return false;
         }
         catch(NumberFormatException e){
             return true;
         }
     }
     
     
     /**Helper methods**/
      public String analyseSearchString(String field, String input, String type){
        String search = "", parameter=" AND";
        
        String[] longerSearch = input.trim().split(" ");
        if(longerSearch.length>1&&((input.trim().indexOf("\"")!=0)||
                (input.trim().indexOf("\"")!=input.length()-1))){
            for(int i=0; i<longerSearch.length;i++){
                if(i==longerSearch.length-1){
                    search +=" i." + field+ " LIKE " + "'%" + longerSearch[i] +"%' " + type; 
                }
                else{
                    search +=" i." + field + " LIKE " + "'%" + longerSearch[i] +"%' " + parameter;
                }
            }
        }
        else{
            search += " i." + field + " LIKE " + "'%" + input +"%' " + type;
        }
       
        return search;
     }
     
     /**End of helpers**/
     /*Main search methods*/
    public String searchItemLocation(String sublocation, String location, String itemid,
            String contact, String quantity){
        //check whether search string is empty, refresh if empty
        String sub = sublocation.replaceAll("[\\s\\u00A0]+$", ""); 
        String loc = location.replaceAll("[\\s\\u00A0]+$", "");
        String itm = itemid.replaceAll("[\\s\\u00A0]+$", "");
        String con = contact.replaceAll("[\\s\\u00A0]+$", "");
        String qty = quantity.replaceAll("[\\s\\u00A0]+$", "");
        if((sub.length()==0) && (loc.length()==0) && (itm.length()==0) &&(con.length()==0) 
                && (qty.length()==0) ){
            recreateModel();
            getItems();
            first =0;
            search =0;
            return "List";
        }
        search =1;            
        normalSearch(sub, loc, itm, con, qty, "AND", "AND", "AND", "AND");
             
         
        return "List";
    }
    
    public void normalSearch(String sublocation, String location, String itemid, 
            String contact, String quantity, String type1, String type2, String type3,
            String type4){
         String search = "SELECT i FROM ItemLocation i WHERE ";
       
         if(sublocation.trim().length()>0){
             search += analyseSearchString("itemLocationPK.sublocation", sublocation, type1);
         }
         
         if(location.trim().length()>0){
             search += analyseSearchString("itemLocationPK.location", location, type2);
         }
         
         if(itemid.trim().length()>0){
             search += analyseSearchString("itemLocationPK.itemid", itemid, type3);
         }
         
         if(contact.trim().length()>0){
             search += analyseSearchString("contact", contact, type4);
         }
         
         if(quantity.trim().length()>0){
             search += analyseSearchString("quantity", quantity, "");
         }
         
         
         
        if(search.trim().endsWith("OR")){
            search = search.trim().substring(0,search.length()-3);
        }
        if(search.trim().endsWith("AND")){
            search = search.trim().substring(0,search.length()-4);
        }
        
        
        tableDataList = entityManager.createQuery(search).getResultList();         //set the page data model
        listLength = tableDataList.size();
        if(listLength>30){
            last=30;
        }
        else{
            last = listLength();
        }
        int tmpLast=0;
        if(listLength()>30){
            tmpLast=29;
        }
        else{
            tmpLast = listLength();
        }
        items.setWrappedData(tableDataList.subList(0, tmpLast));
        recreatePagination();
     }
   
    
    
    
    /**Administrator privileges methods**/
    public boolean isAdmin(){
        
        return true;
    }
    /*End of admin privileges methods*/
    
    /**********************Sign in/Out***************************************/
    
    public ItemLocation getSelecteditem(String itemId) {
        String search = "SELECT i FROM ItemLocation i WHERE i.itemLocationPK.itemid LIKE " 
                + "'%" + itemId +"%'"; 
        ItemLocation queryObject = (ItemLocation) entityManager.createQuery(search).getSingleResult();  
        return queryObject;
    }
    
    public String updateQuantityHelper(ItemLocation itemLocation, int newQty) {
        itemLocation.setQuantity(newQty);
        
        selectedItemIndex = -1;
        try {
            getFacade().edit(itemLocation);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("ItemLocationUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }
    
    
    public ItemLocationFacade qtyFacadeHelper() {
        return ejbFacade;
    }
}
